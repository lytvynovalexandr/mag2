<?php

namespace Perspective\TutorialProductPage\ViewModel;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Perspective\TutorialProductPage\Helper\Custom;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Registry;

class CustomModel implements ArgumentInterface
{
    /**
     * @param Custom $customHelper
     * @param ProductRepositoryInterface $productRepository
     * @param Registry $_coreRegistry
     */
    public function __construct(
        private readonly Custom  $customHelper,
        private readonly ProductRepositoryInterface $productRepository,
        private readonly Registry $_coreRegistry
    )
    {
    }

    /**
     * @return mixed|null
     * @throws NoSuchEntityException
     */
    public function getProduct()
    {
        if (!$this->_coreRegistry->registry('product') && $this->getProductId()) {
            $product = $this->productRepository->getById($this->getProductId());
            $this->_coreRegistry->register('product', $product);
        }
        return $this->_coreRegistry->registry('product');
    }

    /**
     * @return string
     * @throws NoSuchEntityException
     */
    public function getAnyCustomValue()
    {
        $currentProduct = $this->getProduct();
        $customValue = "Any value : ";
        return $customValue . $currentProduct->getFinalPrice();
    }

    /**
     * @return bool
     * @throws NoSuchEntityException
     */
    public function isAvailable()
    {
        $currentProduct = $this->getProduct();
        return $this->customHelper->validateProductBySku($currentProduct->getSku());
    }
}
