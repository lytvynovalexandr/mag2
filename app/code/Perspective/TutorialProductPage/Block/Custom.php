<?php

namespace Perspective\TutorialProductPage\Block;

use Magento\Catalog\Block\Product\View;
use Perspective\TutorialProductPage\Helper\Custom as HelperCustom;
use Magento\Catalog\Block\Product\Context;
use Magento\Framework\Url\EncoderInterface;
use Magento\Framework\Json\EncoderInterface as EncInterface;
use Magento\Framework\Stdlib\StringUtils;
use Magento\Catalog\Helper\Product;
use Magento\Catalog\Model\ProductTypes\ConfigInterface;
use Magento\Framework\Locale\FormatInterface;
use Magento\Customer\Model\Session;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;

class Custom extends View
{
    public function __construct(
        Context $context,
        EncoderInterface $urlEncoder,
        EncInterface $jsonEncoder,
        StringUtils $string,
        Product $productHelper,
        ConfigInterface $productTypeConfig,
        HelperCustom $customHelper,
        FormatInterface $localFormat,
        Session $customerSession,
        ProductRepositoryInterface $productRepository,
        PriceCurrencyInterface $priceCurrency,
        array $data =[])
    {
        $this->customHelper = $customHelper;
        parent::__construct(
            $context,
            $urlEncoder,
            $jsonEncoder,
            $string,
            $productHelper,
            $productTypeConfig,
            $localFormat,
            $customerSession,
            $productRepository,
            $priceCurrency,
            $data
        );
    }

    public function getAnyCustomValue()
    {
        $currentProduct = $this->getProduct();
        $customValue = "Any value : ";
        return $customValue . $currentProduct->getFinalPrice();
    }

    public function isAvailable()
    {
        $currentProduct = $this->getProduct();
        return $this->customHelper->validateProductBySku($currentProduct->getSku());
    }
}
