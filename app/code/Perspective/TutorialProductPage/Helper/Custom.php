<?php

namespace Perspective\TutorialProductPage\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Custom extends AbstractHelper
{
    private $avaliableSku = [
        'MJ01',
        'Mj01',
        'MJ02',
    ];

    public function validateProductBySku($sku)
    {
        if (in_array($sku, $this->getValidSkuArray())) {
            return true;
        } else {
            return false;
        }
    }

    protected function getValidSkuArray()
    {
        return $this->avaliableSku;
    }
}
